import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {HttpService} from '../service/http.service';
import {SelectItem} from 'primeng/api';
import { Settings } from '../domain/settings';

export class PrimeSettings implements Settings {
    constructor(public id?, public enableBicValidation?) {
    }
}

@Component({
    selector: 'app-root',
    templateUrl: './settings.component.html',
    styleUrls: ['./settings.component.css'],
    providers: [HttpService]
})

export class SettingsComponent implements OnInit {
    checked: boolean = false;
    settings: Settings= new PrimeSettings();
    start: Date;
    end: Date;
    isArchiveSuccess = false;
    isArchiveError = false;
    isLoadingSuccess = false;
    isLoadingError = false;
    loadingFile = false;
    invalidRange = false;
    file: File = null;

    constructor(private httpService: HttpService, private router: Router) {
    }

    ngOnInit() {
        // similar with charts model
        this.httpService.getSettings().then(settings => {
            this.settings = settings;
            this.checked=this.settings.enableBicValidation;
        });
    }

    refreshTransactionsPage() {
        window.location.reload();
    }

    onArchive() {
        if (this.start > this.end) {
            this.invalidRange = true;
        }

        this.httpService.archive(this.start, this.end).subscribe(
            response => {
                this.isArchiveSuccess = true;
                this.isArchiveError = false;
            },
            response => {
                this.isArchiveError = true;
                this.isArchiveSuccess = false;
            }
        );
    }

    mark(settings) {
        console.log(this.checked);
      this.settings.enableBicValidation = this.checked;
      this.httpService.changeBicValidation(settings).subscribe(
          response => {
              window.location.reload(); }
      );
    }

    onChange(event) {
        this.file = event.target.files[0];
        this.httpService.uploadLogFile(this.file).subscribe(
            response => {
                this.loadingFile = false;
                this.isLoadingSuccess = true;
                this.router.navigate(['/']);
            },
            response => {
                this.loadingFile = false;
                this.isLoadingError = true;
            }
        );
    }
}
