import {MenuItem} from 'primeng/api';
import {Component, OnInit} from '@angular/core';

@Component({
    selector: 'app-tab-menu',
    templateUrl: './tab-menu.component.html'})
export class TabBarComponent implements OnInit {

    items: MenuItem[];

    ngOnInit() {
        this.items = [
            {label: 'Suspect Hits Catalogue', icon: 'fa fa-fw fa-book', routerLink: ['/logInfo']},
            {label: 'Statistics', icon: 'fa fa-fw fa-book', routerLink: ['/chart']},
            {label: 'User Administration', icon: 'fa fa-fw fa-book', routerLink: ['/users']},
            {label: 'Audit', icon: 'fa fa-fw fa-book', routerLink: ['/audit']},
            {label: 'Maintenance', icon: 'pi pi-fw pi-cog', routerLink: ['/admin']},
            {label: 'Release Notes', icon: 'fa fa-fw fa-book', routerLink: ['/release']},
            {label: 'Logout', icon: 'pi pi-fw pi-power-off', routerLink: ['/logout']}
        ];
    }
}
