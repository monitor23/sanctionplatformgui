import { Component, OnInit } from '@angular/core';
import {User} from '../domain/user';
import {SelectItem} from 'primeng/api';
import { HttpService } from '../service/http.service';

export class PrimeUser implements User {
    constructor(public userName?, public lastName?, public firstName?, public email?) {}
}

@Component({
    selector: 'app-root',
    templateUrl: './user.component.html',
    styleUrls: ['./user.component.css'],
    providers: [HttpService]
})
export class UserComponent implements OnInit {

    displayDialog: boolean;

    user: User = new PrimeUser();

    selectedUser: User;

    newUser: boolean;
    lastName: SelectItem[];

    firstName: SelectItem[];
    users: User[];

    cols: any[];
    isUpdateUserSuccess: boolean;
    isUpdateUserError: boolean;
    isDeleted: boolean;

    constructor(private httpService: HttpService) { }

    ngOnInit() {
        this.httpService.getUsers().then(users => this.users = users);
        this.displayDialog = false;

        this.cols = [
            { field: 'username', header: 'Username' },
            { field: 'lastName', header: 'Last Name' },
            { field: 'firstName', header: 'First Name' },
            { field: 'email', header: 'Email' }
        ];
    }

    showDialogToAdd() {
        this.newUser = true;
        this.user = new PrimeUser();
        this.displayDialog = true;
    }

    save() {
        this.httpService.addUser(this.user).subscribe(
            response => {
                this.isUpdateUserSuccess = true;
                this.displayDialog = false;
                window.location.reload();
            },
            response => {
                this.isUpdateUserError = true;
                this.displayDialog = true;
            }
        );
    }

    delete(){
        console.log(this.selectedUser);
        this.httpService.deleteUser(this.selectedUser).subscribe(
            response =>{
                this.displayDialog = false;
                window.location.reload();
            },
            response =>{
                this.isDeleted = false;
                this.displayDialog = true;
            }
        );
    }

    onRowSelect(event) {
        this.newUser = false;
        this.user = {...event.data};
        this.displayDialog = true;
    }

    findSelectedUserIndex(): number {
        return this.users.indexOf(this.selectedUser);
    }
}
