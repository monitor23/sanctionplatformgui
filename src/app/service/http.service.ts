import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {LogInfo} from '../domain/logInfo';
import {Observable} from 'rxjs';
import {Checked} from '../domain/checked';
import { Doughnut } from '../domain/doughnut';
import { User } from '../domain/user';
import { map } from 'rxjs/operators';
import { Audit } from '../domain/audit';
import { Settings } from '../domain/settings';
import { PrimeSettings } from '../settings/settings.component';
import { formatDate } from '@angular/common';

@Injectable()
export class HttpService {
    
    url = 'http://172.31.31.93:9000/';
    constructor(private http: HttpClient) {}

    getAll() {
        return this.http.get<LogInfo[]>(this.url + 'getAll')
            .toPromise()
            .then(data => data);
    }

    mark(id: string): Observable<any> {
        console.log(id);
        const c = new Checked();
        c.id = id;
        return this.http.post(this.url + 'mark', c);
    }

    uploadLogFile(file): Observable<any> {

        const formData = new FormData();
        formData.append('file', file, file.name);
        return this.http.post(this.url + 'uploadLogFile', formData);
    }

    getDoughnutData() {
        return this.http.get<Doughnut>(this.url + 'statistics')
            .toPromise()
            .then(data => data);
    }

    getUsers() {
        return this.http.get<User[]>(this.url + 'getUsers')
        .toPromise()
        .then(data => data);
    }

    addUser(user: User): Observable<User> {
        return this.http.post(this.url + 'addUser', user);
    }

    deleteUser(selectedUser: User): Observable<any> {
        console.log(this.url+'deleteUser/'+selectedUser.id);
        return this.http.delete(this.url+'deleteUser/'+selectedUser.id);
    }

    login(username: any, password: any): Observable<any>{
        const headers = new HttpHeaders({ Authorization: 'Basic ' + btoa(username + ':' + password) });
      return this.http.get<User>(this.url +'validateLogin',{headers}).pipe(
       map(
         userData => {
          sessionStorage.setItem('username',username);
          return userData;
         }
       )
  
      );
    }

    getAuditInfo() {
        return this.http.get<Audit[]>(this.url + 'getAuditInfo')
        .toPromise()
        .then(data => data);
    }

    changeBicValidation(sett: Settings): Observable<any> {
        
        return this.http.post(this.url + 'changeBicValidationService', sett);
    }

    getSettings() {
        return this.http.get<Settings>(this.url + 'getSettings')
            .toPromise()
            .then(data => data);
    }

    archive(start: Date, end: Date): Observable<any> {
        let params = new HttpParams();

        const startD =  formatDate(start, 	'yyyy-MM-dd', 'en-US');
        const endD =  formatDate(end, 'yyyy-MM-dd', 'en-US');
        console.log(startD);
        console.log(endD);
        params = params.append('start', startD );
        params = params.append('end', endD);
        return this.http.get<any>(this.url + 'archive', {params: params} );
    }
}

