import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { User } from '../domain/user';
import { HttpService } from './http.service';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {


    url = 'http://172.31.31.93:9000/';

    constructor(
        private httpClient:HttpClient
      ) 
    {}
         authenticate(username, password) {
            sessionStorage.setItem('headers',btoa(username + ':' + password))
          const headers = new HttpHeaders({ Authorization: 'Basic ' + btoa(username + ':' + password) });
          return this.httpClient.get<User>(this.url+'validateLogin',{headers}).pipe(
           map(
             userData => {
              sessionStorage.setItem('username', username);
    
              return userData;
             }
           )
      
          );
        }
  


  isUserLoggedIn() {
    let user = sessionStorage.getItem('username')
    console.log(!(user === null))
    return !(user === null)
  }
  getHeader() {
    let header = sessionStorage.getItem('headers')
    return header ;
  }

  logout(){
      sessionStorage.removeItem('username');
      sessionStorage.removeItem('headers');
  }
}