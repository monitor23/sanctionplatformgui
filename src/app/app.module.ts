import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
import {InputTextModule} from 'primeng/inputtext';
import {ButtonModule} from 'primeng/button';
import {TableModule} from 'primeng/table';
import {DialogModule} from 'primeng/dialog';
import {AppComponent} from './app.component';
import {LogInfoComponent} from './logInfo/logInfo.component';
import {DropdownModule} from 'primeng/dropdown';
import {MultiSelectModule} from 'primeng/multiselect';
import {HeaderComponent} from './header/header.component';
import {appRoutingModule} from './app.routing';
import {MessagesModule} from 'primeng/messages';
import {PanelModule} from 'primeng/panel';
import {CardModule} from 'primeng/card';
import {ChartModule} from 'primeng/chart';
import {MenubarModule} from 'primeng/menubar';
import {TabBarComponent} from './tab-bar/tab-bar.component';
import {TabMenuModule} from 'primeng/tabmenu';
import {FooterComponent} from './footer/footer.component';
import {HttpService} from './service/http.service';
import {CalendarModule} from 'primeng/calendar';
import {InputNumberModule} from 'primeng/inputnumber';
import { ChartComponent } from './chart/chart.component';
import { UserComponent } from './user/user.component';
import { LoginComponent } from './login/login.component';
import { AuthenticationService } from './service/authentication.service';
import { AuthGaurdService } from './service/auth-guard.service';
import { BasicAuthInterceptor } from './service/basic.intercerport';
import { AuditComponent } from './audit/audit.component';
import { LogoutComponent } from './logout/logout.component';
import { SettingsComponent } from './settings/settings.component';
import { ReleaseComponent } from './release/release.component';

@NgModule({
    declarations: [
        AppComponent,
        LogInfoComponent,
        ChartComponent,
        UserComponent,
        LoginComponent,
        AuditComponent,
        LogoutComponent,
        SettingsComponent,
        ReleaseComponent,
        HeaderComponent,
        FooterComponent,
        TabBarComponent
    ],
    imports: [
        appRoutingModule,
        BrowserModule,
        BrowserAnimationsModule,
        TableModule,
        HttpClientModule,
        InputTextModule,
        DialogModule,
        ButtonModule,
        DropdownModule,
        MultiSelectModule,
        PanelModule,
        MessagesModule,
        FormsModule,
        CardModule,
        ChartModule,
        MenubarModule,
        TabMenuModule,
        CalendarModule,
        InputNumberModule
    ],
    providers: [HttpService, AuthenticationService, AuthGaurdService,
        { provide: HTTP_INTERCEPTORS, useClass: BasicAuthInterceptor, multi: true }
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
