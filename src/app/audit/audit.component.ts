import {Component, OnInit} from '@angular/core';
import {LogInfo as Audit} from '../domain/logInfo';
import {Router} from '@angular/router';
import {HttpService} from '../service/http.service';
import {SelectItem} from 'primeng/api';

export class PrimeAudit implements Audit {
    constructor(public id?, public bic?, public username?, public state?, public messageType?, public messageRef?,
                public actionDate?) {
    }
}

@Component({
    selector: 'app-root',
    templateUrl: './audit.component.html',
    styleUrls: ['./audit.component.css'],
    providers: [HttpService]
})
export class AuditComponent implements OnInit {
    audit: Audit = new PrimeAudit();

    selectedLogInfo: Audit;
    auditInfos: Audit[];
    cols: any[];

    constructor(private httpService: HttpService, private router: Router) {
    }

    ngOnInit() {
    
        this.httpService.getAuditInfo().then(audits => {
            this.auditInfos = audits;
        });
        this.cols = [
            {field: 'username', header: 'Username'},
            {field: 'state', header: 'Changed state'},
            {field: 'actionDate', header: 'Action date'},
            {field: 'bic', header: 'BIC'},
            {field: 'messageType', header: 'Message type'},
            {field: 'messageRef', header: 'Message ref'}
        ];
    }

}
