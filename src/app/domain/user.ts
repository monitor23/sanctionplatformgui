export interface User {
    id?;
    username?;
    lastName?;
    firstName?;
    password?;
    email?;
}
