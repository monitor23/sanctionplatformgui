export interface LogInfo {
    id?;
    bic?;
    messageType?;
    messageRef?;
    createdDate?;
    checked?;
    bicStatus?;
}
