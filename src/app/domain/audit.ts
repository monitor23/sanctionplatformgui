export interface Audit {
    id?;
    username?;
    state?;
    bic?;
    messageType?;
    messageRef?;
    actionDate?;
    
}
