import {Component, OnInit} from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../service/authentication.service';

@Component({
  selector: 'app-root',
  templateUrl: './logout.component.html'
})
export class LogoutComponent implements OnInit {

  constructor(private router: Router,
    private loginservice: AuthenticationService) { }

  ngOnInit() {
    this.loginservice.logout();
    this.router.navigate(['/login']);
  }
}
