import {RouterModule, Routes} from '@angular/router';
import { AuditComponent } from './audit/audit.component';
import { ChartComponent } from './chart/chart.component';
import { LoginComponent } from './login/login.component';
import {LogInfoComponent} from './logInfo/logInfo.component';
import { LogoutComponent } from './logout/logout.component';
import { ReleaseComponent } from './release/release.component';
import { AuthGaurdService } from './service/auth-guard.service';
import { SettingsComponent } from './settings/settings.component';
import { UserComponent } from './user/user.component';

const routes: Routes = [
    {path: 'login', component: LoginComponent},
    {path: 'logInfo', component: LogInfoComponent, canActivate:[AuthGaurdService]},
    {path: 'chart', component: ChartComponent, canActivate:[AuthGaurdService]},
    {path: 'users', component: UserComponent, canActivate:[AuthGaurdService]},
    {path: 'audit', component: AuditComponent, canActivate:[AuthGaurdService]},
    {path: 'logout', component: LogoutComponent, canActivate:[AuthGaurdService]},
    {path: 'admin', component: SettingsComponent, canActivate:[AuthGaurdService]},
    {path: 'release', component: ReleaseComponent, canActivate:[AuthGaurdService]},
    { path: '',   redirectTo: '/logInfo', pathMatch: 'full' }

];

export const appRoutingModule = RouterModule.forRoot(routes);
