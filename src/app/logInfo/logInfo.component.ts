import {Component, OnInit} from '@angular/core';
import {LogInfo} from '../domain/logInfo';
import {Router} from '@angular/router';
import {HttpService} from '../service/http.service';
import {SelectItem} from 'primeng/api';
import { PrimeSettings } from '../settings/settings.component';
import { Settings } from '../domain/settings';

export class PrimeLogInfo implements LogInfo {
    constructor(public id?, public bic?, public bicStatus?, public messageType?, public messageRef?,
                public createdDate?, public checked?) {
    }
}

@Component({
    selector: 'app-root',
    templateUrl: './logInfo.component.html',
    styleUrls: ['./logInfo.component.css'],
    providers: [HttpService]
})
export class LogInfoComponent implements OnInit {
    checked: SelectItem[];
    displayDialog: boolean;

    logInfo: LogInfo = new PrimeLogInfo();

    selectedLogInfo: LogInfo;
    logInfos: LogInfo[];
    cols: any[];

    checkedBic: boolean = false;
    settings: Settings = new PrimeSettings();


    constructor(private httpService: HttpService, private router: Router) {
    }

    ngOnInit() {
        // similar with charts model
        this.httpService.getAll().then(logs => {
            this.logInfos = logs;
        });

        this.httpService.getSettings().then(settings => {
            this.settings = settings;
            this.checkedBic = this.settings.enableBicValidation;
            if ( this.checkedBic == true ) {
                this.cols = [
                    {field: 'bic', header: 'BIC'},
                    {field: 'bicStatus', header: 'BIC Status'},
                    {field: 'messageType', header: 'Message type'},
                    {field: 'messageRef', header: 'Message ref'},
                    {field: 'createdDate', header: 'Created date'},
                    {field: 'checked', header: 'Verified'}
                ];
            }
            else {
                this.cols = [
                    {field: 'bic', header: 'BIC'},
                    {field: 'messageType', header: 'Message type'},
                    {field: 'messageRef', header: 'Message ref'},
                    {field: 'createdDate', header: 'Created date'},
                    {field: 'checked', header: 'Verified'}
                ];
            }
        });
        this.checked = [
            {label: 'All', value : null},
            {label: 'Verified', value : 'true'},
            {label: 'Not verified', value : 'false'}
        ];
    }

    onRowSelect(event) {
        this.logInfo = {...event.data};
        this.displayDialog = true;
    }

    refreshTransactionsPage() {
        window.location.reload();
    }

    mark(logInfo) {
      this.httpService.mark(logInfo.id).subscribe(
          response => {
        this.ngOnInit(); }
      );
    }
}
