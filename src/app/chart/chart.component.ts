import {Component, OnInit} from '@angular/core';
import {Doughnut} from '../domain/doughnut';
import {HttpService} from '../service/http.service';

export class PrimeDoughnut implements Doughnut {
    constructor(public verifiedCount?, public notVerifiedCount?) {
    }
}

@Component({
    selector: 'app-root',
    templateUrl: './chart.component.html',
    styleUrls: ['./chart.component.css'],
    providers: [HttpService]
})
export class ChartComponent implements OnInit {

    doughnutChartData: any;

    msgs: any[];

    doughnut: Doughnut = new PrimeDoughnut();
   

    constructor(private httpService: HttpService) {

    }

    ngOnInit() {
        this.httpService.getDoughnutData().then(doughnut => {
            this.doughnut = doughnut;
            this.doughnutChartData = {
                labels: ['NotVerified', 'Verified'],
                datasets: [
                    {
                        data: [this.doughnut.notVerifiedCount, this.doughnut.verifiedCount],
                        backgroundColor: [
                            '#ffb6c1',
                            '#707d90',
                            '#FFCE56'
                        ],
                        hoverBackgroundColor: [
                            '#ffb6c1',
                            '#708090',
                            '#FFCE56'
                        ]
                    }]
            };
        });
        this.msgs = [];


    }
}
